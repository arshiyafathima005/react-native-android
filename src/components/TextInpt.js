import React, { useState } from 'react';
import { Text, TextInput,StyleSheet } from 'react-native';

const TextInpt=()=>{
    const[email,setEmail]=useState('');
    return(
    
          <TextInput
          style={styles.inputTxt}
          placeholder="Email"
          onChangeText={email=>setEmail(email)}
          defaultValue={email}>
          </TextInput>  
        
    );
}
const styles=StyleSheet.create(
    {
        inputTxt:{
            height:30,
            backgroundColor:'azure',
            fontSize:16,
            padding:16,
            color:'blue',
            marginLeft:10,
            marginRight:10,
            borderWidth:1,
            borderColor:'blue',
            borderRadius:10
            }
    }
);