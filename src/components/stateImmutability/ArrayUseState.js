import React,{useState} from 'react';
import {Text,StyleSheet,TouchableOpacity} from 'react-native';

const firstState=['Arshiya','Fathima']
const ArrayUseState=()=>{
    const [person,setPerson]=useState(firstState)
    return(
        <TouchableOpacity style={styles.touchBtn} 
        onPress={change}>
        <Text style={{textAlign:'center',color:'white'}}>{person.firstName} {person.lastName}</Text>
        </TouchableOpacity>
    )
}
export default ArrayUseState;

const styles=StyleSheet.create(
    {
        touchBtn:
        {
        marginTop:10,
        backgroundColor:'#00BCD4',
        borderRadius:10
        ,padding:10
        }
});
