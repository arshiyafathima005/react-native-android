import React,{useState} from 'react';
import {Text,StyleSheet,TouchableOpacity} from 'react-native';

const firstState={
    firstName:'Arshiya',
    lastName:'Fathima'
}
const ObjectImmutability=()=>{
    const [person,setPerson]=useState(firstState)

    const change=()=>{
       // person.firstName='Mohammed'
       // person.lastName='Ziya'
       //setPerson(person);
       const personNew={...person}
       {
           personNew.firstName='Mohammed'
           personNew.lastName='Ziya'
           setPerson(personNew);
       }
    }
    return(
        <TouchableOpacity style={styles.touchBtn} 
        onPress={change}>
        <Text style={{textAlign:'center',color:'white'}}>{person.firstName} {person.lastName}</Text>
        </TouchableOpacity>
    )
}
export default ObjectImmutability;

const styles=StyleSheet.create(
    {
        touchBtn:
        {
        marginTop:10,
        backgroundColor:'#00BCD4',
        borderRadius:10
        ,padding:10
        }
});