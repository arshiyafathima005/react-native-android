import React from 'react';
//import {StyleSheet} from 'react-native';
import {Provider as PaperProvider,ActivityIndicator,Colors} from 'react-native-paper';

//size we can give small,large,numbers we can
const Indicators=()=>
{
    return(
<ActivityIndicator 
animating={true} color={Colors.deepPurpleA400} 
size={50}
></ActivityIndicator>
    )
}
export default Indicators;