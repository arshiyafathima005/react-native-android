import React, {useState} from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';

import {
  Dialog,
  Portal,
  Button,
  Paragraph,
  Avatar,
  Divider,
  Card,
  Text,
  Chip,
  FAB,
  Provider,
  DataTable,
  ProgressBar,
  Colors
 } from 'react-native-paper';

const Dailogs = () => {
  const [visible, setDailogVisible] = useState(false);
  const showDailog = () => setDailogVisible(true);
  const closeDailog = () => setDailogVisible(false);

  return (
    <View>
      <Card style={styles.card}>
        <Card.Actions>
          <Button onPress={showDailog}>Show Details</Button>
        </Card.Actions>
        <Divider />
        <Text style={{margin: 20}}>Your Progress</Text>
        <ProgressBar
          //progress={0.7}
          color={Colors.purple300}
          style={{margin: 20}}
          indeterminate={true}
        />
        <Divider />
       <DataTables />
        <Divider />
       <Text style={styles.text}>Choose Submition Type</Text>
        <View style={styles.chip}>
          <Chip type="flat" style={{marginHorizontal: 5}}>
            <Avatar.Icon size={25} icon="access-point-network" />
            <Text>Online</Text>
          </Chip>
          <Chip
            type="flat"
            accessibilityLabel="Online"
            closeIconAccessibilityLabel="Close"
            style={{marginHorizontal: 5}}>
            <Avatar.Icon size={25} icon="flash-circle" />
            <Text>Offline</Text>
          </Chip>
          <Divider />
        </View>
        <Fab />
      </Card>
      <Portal>
        <Dialog
          visible={visible}
          // onDismiss={closeDailog}
          dismissable={false}>
          <View style={{flexDirection: 'row'}}>
            <Avatar.Image
              size={60}
              source={{
                uri:
                  'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8aHVtYW58ZW58MHx8MHw%3D&ixlib=rb-1.2.1&w=1000&q=80',
              }}
              style={{marginVertical: 10, marginLeft: 20}}
            />
            <Dialog.Title>Alert</Dialog.Title>
          </View>
          <Divider inset={true} />
          <Dialog.Content>
            <Paragraph>
              Dialogs inform users about a specific task and may contain
              critical information, require decisions, or involve multiple
              tasks.
            </Paragraph>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={closeDailog}>Cancle</Button>
            <View style={styles.verticalLine} />
            <Button onPress={closeDailog}>ok</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
     
    </View>
  );
};

const Fab = () => {
  const [state, setState] = useState({open: false});
  const onStateChange = ({open}) => setState({open});
  const {open} = state;
  return (
    <Provider>
      <Portal>
        <FAB.Group
          open={open}
          style={styles.fab}
          small={false}
          color="#fff"
          // animated={true}
          // loading={true}
          //visible={true}
          fabStyle={{backgroundColor: '#6c22ee'}}
          icon={open ? 'calendar-today' : 'plus'}
          actions={[
            {
              icon: 'minus',
              onPress: () => console.log('Pressed add'),
              color: '#9900ef',
            },
            {
              icon: 'star',
              label: 'Rate',
              onPress: () => console.log('Pressed star'),
              color: '#9900ef',
            },
            {
              icon: 'bell',
              label: 'Remind',
              onPress: () => {},
              small: false,
              color: '#9900ef',
            },
          ]}
          onStateChange={onStateChange}
          onPress={() => {}}
        />
      </Portal>
    </Provider>
  );
};

const Modals = () => {
  const [modal, setModalVisible] = useState(false);
  const showModal = () => setModalVisible(true);
  const closeModal = () => setModalVisible(false);

  const containerStyle = {backgroundColor: 'white', padding: 20};

  return (
    <Provider>
      <Portal>
        <Modal
          visible={modal}
          onDismiss={closeModal}
          contentContainerStyle={containerStyle}>
          <Text>Example Modal. Click outside this area to dismiss.</Text>
        </Modal>
      </Portal>
      <Button style={{marginTop: 20}} onPress={showModal}>
        Show
      </Button>
    </Provider>
  );
};
const itemPerPage = 2;
const items = [
  {
    key: 1,
    name: 'page 1',
  },
  {
    key: 2,
    name: 'page 2',
  },
  {
    key: 3,
    name: 'page 3',
  },
];
const DataTables = () => {
  const [page, setPage] = useState(0);
  const from = page * itemPerPage;
  const to = (page + 1) * itemPerPage;
  return (
    <DataTable style={styles.dataTable}>
      <DataTable.Header>
        <DataTable.Title sortDirection="descending">Name</DataTable.Title>
        <DataTable.Title>Email</DataTable.Title>
        <DataTable.Title numeric>Age</DataTable.Title>
      </DataTable.Header>

      <DataTable.Row>
        <DataTable.Cell>Arshiya</DataTable.Cell>
        <DataTable.Cell>arshiya@gmail.com</DataTable.Cell>
        <DataTable.Cell numeric>21</DataTable.Cell>
      </DataTable.Row>

      <DataTable.Row>
        <DataTable.Cell>Fathima</DataTable.Cell>
        <DataTable.Cell>fathima@gmail.com</DataTable.Cell>
        <DataTable.Cell numeric>23</DataTable.Cell>
      </DataTable.Row>
      <DataTable.Row>
        <DataTable.Cell>No of rows per page</DataTable.Cell>
        <DataTable.Pagination
          page={page}
          numberOfPages={Math.floor(items.length / itemPerPage)}
          onPageChange={page => {
            page => setPage(page);
          }}
          label={`${from + 1}-${to} of ${items.length}`}
        />
      </DataTable.Row>
    </DataTable>
  );
};

/*const SnackBars=()=>{
  const[snackbar,setSnackbar]=useState(false);
  const openSnackBar=()=>setSnackbar(!snackbar);
  const onCloseSnackBar=()=>setSnackbar(false);
  return(
    <View>
      <Button onPress={openSnackBar}>{snackbar?'Hide':'Show'}</Button>
      <SnackBars
      visible={snackbar}>
        SnackBars
      </SnackBars>
    </View>
  )
}*/
export default Dailogs;
const styles = StyleSheet.create({
  verticalLine: {
    height: '70%',
    width: 0.4,
    backgroundColor: '#909090',
    margin: 10,
  },
  card: {
    width: 340,
    height: '97%',
    borderRadius: 10,
    margin: 10,
    borderWidth: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    elevation: 7,
  },
  text: {
    fontSize: 20,
    padding: 10,
    fontWeight: 'bold',
  },
  chip: {
    flex: 1,
    margin: 4,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
    color: '#9900ef',
  },
  dataTable: {
    padding: 5,
    margin: 1,
    flexWrap: 'wrap',
  },
});
