import React,{useState} from 'react';
import {StyleSheet,View, ImageBackground, ScrollView} from 'react-native';
import {TextInput,Text,Checkbox,List,Divider,RadioButton, Switch} from 'react-native-paper';

const Forms=()=>
{
    return(
       <ScrollView stickyHeaderIndices={[0]} >
            <ImageBackground
                    style={styles.image}
                    source={{
                     uri:'https://www.betterteam.com/i/new-hire-forms-422x320-20170919.jpg',
                     }} >
                    <Text style={styles.text}>APPLICATION FORM</Text> 
            </ImageBackground>
           <TextInp />
            <Lists/>
           <Radios/>
           <Switchs/>
       </ScrollView>
            
       
        
    );
}
const TextInp=()=>
{
    /*const { values, handleChange, reset } = useForm({ username: '', password: '' });
    const handleSubmit = e => {
        e.preventDefault();
        reset();
    };*/

    const [text,setText]=useState('');
    const [email,setTextEmail]=useState('');
    const [number,setNumber]=useState(0);
    return(
       
            <View >
               <TextInput
                         label="First Name"
                         //value={text}
                         mode='outlined'
                         //onChangeText={text=>setText(text)}  
                         style={styles.textInptStyles}
                         autoCapitalize='words'
                         autoCompleteType='name' />
                    <TextInput
                         label="Last Name"
                         //value={text}
                         mode='outlined'
                         
                         //onChangeText={text=>setText(text)}  
                         style={styles.textInptStyles} />
                     <TextInput
                        label="Email"
                        value={email}
                        mode='outlined'
                        dense={false}
                        onChangeText={text=>setTextEmail(text)} 
                        //secureTextEntry={true} for password or
                         //password={true} 
                        style={styles.textInptStyles}
                        right={ <TextInput.Icon color="#8C8C8C" icon="email" size={30}/>  }
                        selectionColor="#9900ef"/>
                    <TextInput
                        label="Phone Number"
                        keyboardType='numeric'
                        mode='outlined'
                        maxLength={10}
                        style={styles.textInptStyles}
                        left={ <TextInput.Affix color text="+91" size={30}/> }
                        right={ <TextInput.Icon color="#8C8C8C" icon="phone" size={30}/> }
                         />
                    <Text style={styles.textForm}>Gender*</Text> 
                    <CheckBoxes />  
                    <TextInput
                         label="Address"
                        //value={text}
                        mode='outlined'
                        style={styles.textInptStyles}
                    right={ <TextInput.Icon color="#8C8C8C" icon="home" size={30}/> }
                         />  
            </View>
       
    );
}
const CheckBoxes = () => {
    const [checked, setChecked] = useState(false);
    const [checked2, setChecked2] = useState(false);
    const [checked3, setChecked3] = useState(false);
  
    return (
        <View style={{ flexDirection: 'column',marginLeft:20}}>
           <View style={{ flexDirection: 'row' }}>
            <View style={{ flexDirection: 'row' }}>
                <Checkbox
                    style={styles.checkbox}
                    status={checked ? 'checked' : 'unchecked'}
                    onPress={() => {
                    setChecked(!checked);
                    }}
                    uncheckedColor="#9900ef"/> 
                 <Text style={{marginTop: 5}}>MALE</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
                <Checkbox 
                    style={styles.checkbox}
                    status={checked2 ? 'checked' : 'unchecked'}
                    onPress={() => {
                    setChecked2(!checked2);
                    }}
                    uncheckedColor="#9900ef"/> 
                <Text style={{marginTop: 5}}>FEMALE</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
                <Checkbox 
                    style={styles.checkbox}
                    status={checked3 ? 'checked' : 'unchecked'}
                    onPress={() => {
                    setChecked3(!checked3);
                    }}
                    uncheckedColor="#9900ef"/> 
                <Text style={{marginTop: 5}}>OTHERS</Text>
            </View>
          </View>
        </View>
         
    );
  };
  const Lists=()=>{
    const [expanded, setExpanded] = React.useState(true);
    const handlePress = () => setExpanded(!expanded);
   return (
      <List.Section title="Address Proof">
       <List.Accordion
        title="Minor"
        left={props => <List.Icon {...props} icon="folder" color="#9900ef"/>}>
        <Divider/>
        <List.Item title="Addhar" />
        <Divider/>
        <List.Item title="Birth Certificate" />
      </List.Accordion>

      <List.Accordion
        title="Major" titleStyle={{margin:10}}
        left={props => <List.Icon {...props} icon="folder" color="#9900ef"/>}
        expanded={expanded}
        onPress={handlePress}>
        <Divider/>
        <List.Item title="Addhar" left={() => <List.Icon icon="credit-card" color="#d4c4fb"/>} />
        <Divider/>
        <List.Item title="ID Card" left={() => <List.Icon icon="pan" color="#d4c4fb"/>}/>
        <Divider/>
        <List.Item title="Driving Licence" left={() => <List.Icon icon="card" color="#d4c4fb"/>}/>
        <Divider/>
        <List.Item title="National ID" left={() => <List.Icon icon="id-card" color="#d4c4fb"/>} />
        <Divider/>
        <List.Item title="Passport" left={() => <List.Icon icon="passport" color="#d4c4fb"/>}/>
        <Divider/>
        <List.Item title="Pan Card" left={() => <List.Icon icon="pan" color="#d4c4fb"/>}/>
      </List.Accordion>
    </List.Section>
  );
  }

  const Radios=()=>
  {
    const[checkeRadio,setRadioChecked]=useState('first')
    return(
      <View >
          <Text style={styles.textNormal}>If you cancel this process all your data will be lost. Really cancel? </Text>
          <RadioButton.Group 
            onValueChange={valueNew=>setRadioChecked(valueNew)} 
            value={checkeRadio}
           >
            <View style={{ flexDirection: 'row' }}>
              <RadioButton
                value="first"
               // status={checkeRadio==='first'?'checked':'unchecked'} without group check status
                //onPress={()=>setRadioChecked('first')} 
                uncheckedColor="#2ccce4"
                color="#9013fe"
                />
              <Text>Yes</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <RadioButton 
                value="second"
                //status={checkeRadio==='second'?'checked':'unchecked'}
                //onPress={()=>setRadioChecked('second')}
                uncheckedColor="#2ccce4"
                color="#9013fe"
                />
              <Text>No</Text>
            </View>
        </RadioButton.Group>
      </View>
    );
  }
  const Switchs=()=>{
    const [switchOn,setSwitchOn]=useState(false);
    return(
        <View style={{flexDirection:'row'}}>
          <Text style={styles.textNormal}>Save your entered details by toggeling</Text>
          <Switch 
          value={switchOn}
          onValueChange={()=>setSwitchOn(!switchOn)}
          //disabled={true}
          color="#9013fe"/>
        </View>
    );
  }


   /*const useForm = (initialValues) => {
    const [values, setValues] = useState(initialValues);
  
    return {
      values,
      handleChange: (e) => {
        setValues({
          ...values,
          [e.target.label]: e.target.value,
        });
      },
      reset: () => setValues(initialValues),
    };
  };*/
export default Forms;
const styles=StyleSheet.create({
    textInptStyles:{
      marginLeft:20,
      marginRight:20,
      marginBottom:20,
      height:50,
      fontSize:16
     
    },
    text: {
        flex:1,
        color: "white",
        fontSize: 25,
        fontWeight: "bold",
        textAlign: "center",
        marginTop:20,
        marginBottom:20,
        justifyContent:"center"
        
     },
     textNormal:{
        flex:1,
        justifyContent:"center",
        alignItems: 'center',
        margin:10,
        fontWeight:"bold"
     },
     image: {
        flex: 1,
        justifyContent: "center",
        marginBottom:30
      },
      textForm:{
          marginLeft:20
      },
      checkbox:{
          marginLeft:20
      }
 });
