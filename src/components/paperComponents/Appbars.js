import React from 'react';
import {StyleSheet} from 'react-native';
import {Appbar} from 'react-native-paper';
import App from '../../../App';

const Appbars=()=>{
    

  const _search = () => console.log('Searching');

  const _more = () => console.log('Shown more');
    return(
        <Appbar.Header >
            <Appbar.BackAction />
            <Appbar.Content title="Practice Paper" subtitle="First App" />
            <Appbar.Action icon="magnify" onPress={_search} />
             <Appbar.Action icon="dots-vertical" onPress={_more} />
     </Appbar.Header>
    
      
    );
}
const AppBottom=()=>{
    return(
        <Appbar style={styles.bottom}>
        <Appbar.Action
          icon="archive"
          onPress={() => console.log('Pressed archive')}
         />
         <Appbar.Action icon="mail" onPress={() => console.log('Pressed mail')} />
         <Appbar.Action icon="label" onPress={() => console.log('Pressed label')} />
         <Appbar.Action
           icon="delete"
           onPress={() => console.log('Pressed delete')}
           />
           <Appbar.Action
           icon="pen"
           onPress={()=>console.log('pressed add')}
           />
       </Appbar>
    );
}
export default Appbars;
const styles = StyleSheet.create({
    bottom: {
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
    },
    colr:{
        backgroundColor:'#03DAC5'
    }
  });

