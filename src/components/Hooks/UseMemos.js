import React,{useState,useMemo} from 'react';
import { Button,Text,View } from 'react-native';

const UseMemos=()=>{
const[number,setNumber]=useState(0)
const[multiples,setMultiples]=useState(10);

const countHandler=()=>{
    setNumber(number+1);
}
const muliptyHandler=()=>
{ 
    setMultiples(multiples*10);
}
//useMemo allows you to memoize the results of a function, and will return that result until an array of dependencies change.
const multicountMemo=useMemo(function multicount()//use memo tells this funtion when to update(re-render)
{ console.warn('multiple count');
    return number*5;
},[number]);//adding in condition array on which condition it has to update (here when number is updated it has to update)(array dependencies)

return(
    <View>
        <Text> MEMO {'\n'} Count: {number}</Text>
        <Text>Multiples:{multiples}</Text>
        <Text>Multiple counts:{multicountMemo}</Text>
        <Button title='Memo Button'
        onPress={countHandler}/>
         <Button title='Multiply'
        onPress={muliptyHandler}/>
    </View>
);
}
export default UseMemos;