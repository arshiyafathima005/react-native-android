import React, {useReducer} from 'react';
import {View,Text,Button,} from 'react-native';
const initialState=0;
const reducer=(state,action)=>
{
    switch(action)
    {
        case 'increment':
         return state+1;
         case 'decrement':
         return state-1;
         case 'reset':return initialState;
         default:return state;
        
    }
}
const UseReducers=()=>{
 const [count,dispatch]=useReducer(reducer,initialState)
 //console.log('use reducer');
 return(
     <View>
         <Text>{count}</Text>
         <Button title='Increment'
         onPress={()=>dispatch('increment')}/>
         <Button title='decrement'
          onPress={()=>dispatch('decrement')}/>
         <Button title='Reset'
         onPress={()=>dispatch('reset')}/>
   
     </View>
 );
}
export default UseReducers;

