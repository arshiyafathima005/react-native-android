import React from 'react';
import {Text,TouchableOpacity,StyleSheet,View} from 'react-native';

const FunctionalComp=(props)=>
{  
  return(
    <View>
    <TouchableOpacity style={styles.touchBtn}>
    <Text style={{textAlign:'center',color:'white'}}>Login With Facebook</Text>
    </TouchableOpacity>
 <Text>Hello {props.name}</Text>
 </View> 
);
}
export default FunctionalComp;
const styles=StyleSheet.create(
  {
    touchBtn:
  {
  marginTop:10,
  backgroundColor:'#00BCD4',
  borderRadius:10
  ,padding:10
  }
  }
);