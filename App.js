import React, {useState} from 'react';
import {StyleSheet,View} from 'react-native';
import {
  Provider as PaperProvider,
  BottomNavigation,
  Text,
  Button,
  Appbar,
  Menu,
  Provider,
  Divider,
} from 'react-native-paper';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {HideWithKeyboard} from 'react-native-hide-with-keyboard';
import Forms from './src/components/paperComponents/Forms';
import Appbars from './src/components/paperComponents/Appbars';
import {StackActions} from 'react-navigation';
import Dialogs from './src/components/paperComponents/Dialogs';
import Cards from './src/components/paperComponents/Cards'
export default function App() {
  const Stack = createStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{
          header: props => <CustomNavBar {...props} />,
        }}>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Details" component={DetailsScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
function HomeScreen({navigation}) {
  return (
    <PaperProvider>
      <Forms />
      <HideWithKeyboard>
        <Button onPress={() => navigation.navigate('Details')}>Details</Button>
      </HideWithKeyboard>
    </PaperProvider>
  );
}
function DetailsScreen() {
  return (
    <PaperProvider>
         <Dialogs/>
        
    </PaperProvider>
  
  )
}

function CustomNavBar({navigation, previous}) {
  const _search = () => console.log('Searching');
  const _more = () => console.log('Shown more');
  
  return (
    
    <Appbar.Header>
      {previous ? <Appbar.BackAction onPress={navigation.goBack} /> : null}
      <Appbar.Content title="Practice" subtitle="First App"  />
      <Appbar.Action icon="magnify" onPress={_search} style={{marginLeft:180}} />
      {!previous ? (
       <Menus/>
      ) : null}
     </Appbar.Header>
    );
}

const Menus=()=>{
  const[visible,setVisible]=useState(false);
  const openMenu=()=>setVisible(true)
  const closeMenu=()=>setVisible(false)
  return(
    <Provider >
    <View
      style={{
        flexDirection:'row',
        justifyContent:'center'
      }}>
    <Menu
      visible={visible}
      onDismiss={closeMenu}
      anchor={<Appbar.Action icon="dots-vertical" color="#ffffff" onPress={openMenu} />}>
      <Menu.Item title="Home" icon="home" onPress={() => {console.log('Home was pressed')}}/>
      <Menu.Item title="Details" icon="details" onPress={() => {console.log('Details 1 was pressed')}}/>
      <Menu.Item title="Upload" icon="upload" onPress={()=>{}}/>
      <Menu.Item title="Save" icon="cloud" onPress={() => {console.log('Add 1 was pressed')}}/>
      <Divider/>
      <Menu.Item title="Delete" icon="delete" disabled="true" onPress={()=>{}}/>
    </Menu>
    </View>
  </Provider>
  )
}

/*import ActivityIndicators from './src/components/paperComponents/ActivityIndicators';
import Appbars from './src/components/paperComponents/Appbars';
import BottomNav from './src/components/paperComponents/BottomNav';
import Cards from './src/components/paperComponents/Cards';
import PaperButton from './src/components/paperComponents/PaperButtons';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
export default function App(){
 return(
    <PaperProvider >
     <Appbars/>
     <Cards/>
     <PaperButton/>
     
      <BottomNav/>

  
    </PaperProvider>
  );
}
const styles = StyleSheet.create({
  bottom: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
  },
});*/

/*import React, {useState} from 'react';
import {View,Text,Button,StyleSheet } from 'react-native';
import UseReducers from './src/components/UseReducers';
import ObjectImmutability from './src/components/stateImmutability/ObjectImmutability';
import Parent from './src/components/optimaization/Parent'; 
import Child from './src/components/optimaization/Child'; 
import UseMemos from './src/components/Hooks/UseMemos';
import { ScrollView } from 'react-native-gesture-handler';
import Props from './src/components/Props';
export default function UseStates(){
    const [name,setName]=useState('Arshiya');
    const [obj,objText]=useState({name:'ziya',age:'27'});
    const [count,setCount]=useState(0);
 console.log('use state');
    const clickBtnHandler=()=>
    { 
      setName('Fathima');//setter function which calls the elements to rerender
      objText({name:'Mohammed ziya ulla',age:28})
      setCount(count=>count+1);
    }
    return(
      <ScrollView>
        <View style={styles.container}>
           <Text>My name is {name}</Text>
            <Text>Husband name is {obj.name} and age is {obj.age}</Text>
           <Text>You Clicked {count} times</Text>
            <View style={styles.btnContainer}>
                <Button title='update'
                onPress={clickBtnHandler}/>
                <UseReducers></UseReducers>
                <ObjectImmutability></ObjectImmutability>
                <Parent><Child/></Parent>
                <UseMemos></UseMemos>
            </View>
        </View>
        </ScrollView>
       
    );
}
const styles=StyleSheet.create(
    {
        container:{
            flex:1,
            alignItems:'center',
            justifyContent:'center',
            backgroundColor:'#fff'
        },
        btnContainer:{
          marginTop:20
        }
     });*/

/*import React from 'react';
import{

} from 'react-native';
import UseStates from './src/components/UseStates';
const Func=()=>
{
  return(
  <UseStates/>
  );
}*/
/*import React from 'react';
import{
  StyleSheet,Text,View,TextInput,
Button,Alert,Image,TouchableOpacity,ScrollView
} from 'react-native';
import FunctionalComp from './src/components/FunctionalComp';
import LinearGradient from 'react-native-linear-gradient';
//import TextInput from './src/components/TextInput';
//import {useNavigation} from '@react-navigation/native';

const Func=()=>
{
  return(
    <ScrollView>
    <View>
    <Text style={styles.welcome}>
    Welcome
    </Text>
    <View style={{alignItems:'center'}}>
    <Image 
    source={{uri:'https://www.kindpng.com/picc/m/495-4952535_create-digital-profile-icon-blue-user-profile-icon.png'}}
    style={{width:80,height:80,borderRadius:50}}/>
    </View>
    <Text style={{padding:10,color:'blue',fontSize:16}}>Username:</Text>
    <TextInput style={styles.inputTxt}
    placeholder="Email">
    </TextInput>
    <Text style={styles.txt}>Password:</Text>
    <TextInput style={styles.inputTxt}
    placeholder="Password"
    autoCompleteType='password'>
    </TextInput>
    <Text style={{margin:10}}>Forgot Password?</Text>
    <View style={styles.btnView}>
    <Button 
    title="LOGIN"
    onPress={()=>Alert.alert('Successful login')}
    />
    <Next on></Next>
    <FunctionalComp name="asvi"></FunctionalComp>
    <FunctionalComp name="sufi"></FunctionalComp>
    <FunctionalComp name="arshi"></FunctionalComp>
    </View>
    <LinearGradient
        colors={['blue', 'white', 'azure' ]}
        style={styles.linearGradient}
        start={{ x: 0, y: 0.5 }}
        end={{ x: 1, y: 0.5 }}
      >
        <Text>Horizontal Gradient</Text>
      </LinearGradient>
    </View>
    </ScrollView>);
}
export default Func;
const Next=()=>
{
  return(
    <TouchableOpacity style={styles.touchBtn} 
    onPress={Display}>
    <Text style={{textAlign:'center',color:'white'}}>Login With Google</Text>
    </TouchableOpacity>
   
  );
  function Display()
  {
    Alert.alert('Coming Soon');
  }
}
const styles=StyleSheet.create(
  {
  welcome:{
  fontSize:30,
  textAlign:'center',
  margin:10,
  color:'blue',
  backgroundColor:'azure',
  },
  inputTxt:{
  height:40,
  backgroundColor:'azure',
  fontSize:16,
  padding:10,
  color:'blue',
  marginLeft:10,
  marginRight:10,
  borderWidth:1,
  borderColor:'blue',
  borderRadius:10
  },
  txt:{
  padding:10,
  color:'blue',
  fontSize:16
  },
  btnView:{
  margin:10,
  },
  touchBtn:
  {
  marginTop:10,
  backgroundColor:'#00BCD4',
  borderRadius:10
  ,padding:10
  }
  }
);*/
/*import React, { Component } from 'react';
//import react in our code. 

//Import react-navigation
import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';

import FirstPage from './src/components/FirstPage';
import SecondPage from './src/components/SecondPage';
//import all the screens we are going to switch 
const App = createStackNavigator({
  //Constant which holds all the screens like index of any book 
    FirstPage: { screen: FirstPage }, 
    //First entry by default be our first screen if we do not define initialRouteName
    SecondPage: { screen: SecondPage }, 
  },
  {
    initialRouteName: 'FirstPage',
  }
);
export default createAppContainer(App);*/
